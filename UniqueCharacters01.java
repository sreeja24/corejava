import java.util.*;
public class UniqueCharacters01 {
    public static void main(String[] args) {
        String str = "ENGINEERING";
        Set<Character> uniqAlphs = getUniqueLetters(str);
        System.out.println(uniqAlphs);
    }

    public static Set<Character> getUniqueLetters(String str) {
	TreeSet<Character> Letters = new TreeSet<Character>();

		if ( str == null || str.isEmpty())
			return Letters;
		if (str.charAt(0) == ' ' || str.charAt(str.length() - 1) == ' ')
			str.trim();

		for (int i = 0; i < str.length(); i++) {

			if(Character.isAlphabetic(str.charAt(i)) || Character.isDigit(str.charAt(i)))
				Letters.add(str.charAt(i));
			
		}
		
	
		return Letters;

    }
}
