import java.util.*;
public class CharFrequency01 {
	public static void main(String[] args) {
		Map<Character, Integer> freqCount = frequencyCount("ELEMENTS");
        System.out.println(freqCount);
        
        for (Character key : freqCount.keySet()) {
            System.out.println(key + " " + freqCount.get(key));
        }  
        
	}
    public static Map<Character, Integer> frequencyCount(String str) {
      TreeMap map = new TreeMap<Character, Integer>();
		char c ;

		if (str.isEmpty() || str == null)
			return map;

		if (str.charAt(0) == ' ' || str.charAt(str.length() - 1) == ' ')
			str.trim();

		for (int i = 0; i < str.length(); i++) {
			
			if (Character.isLowerCase(str.charAt(i))) {
			str = str.replace(str.charAt(i), Character.toUpperCase(str.charAt(i)));
				
			}
			

		}
		
		

		for (int i = 0; i < str.length(); i++) {

			if (Character.isAlphabetic(str.charAt(i))) {

				if (map.containsKey(str.charAt(i))) {
					Integer v = (Integer) map.get(str.charAt(i));
					v++;
					map.put(str.charAt(i), v);

				}

				else
					map.put(str.charAt(i), 1);

			}

		}

		return map;
    }
}
