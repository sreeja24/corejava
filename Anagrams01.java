import java.util.*;

public class Anagrams01 {
	public static void main(String[] args) {
		String[] words = { "listen", "silent", "elbow", "PART", "panel", "trap", "tensil", "alter", "later", "below" };
		List<String> anagrams = findAnagrams(words);
		for (String word : anagrams) {
			System.out.println(word);
		}
	}

	public static List<String> findAnagrams(String[] words) {
		String words1[] = new String[words.length];
		for (int i = 0; i < words.length; i++) {
			words[i] = words[i].toLowerCase();
			words1[i] = words[i];
		}

		List<String> list = new ArrayList<String>();
		String anagram = "";
		int c = 0;

		if (words.equals(null))
			return list;
		for (int j = 0; j < words.length; j++) {
			char strWord[] = words[j].toCharArray();
			Arrays.sort(strWord);
			String str = new String(strWord);
			words[j] = str;
			
		}

		for (int i = 0; i < words.length; i++) {
			anagram = "";
			c = 0;
			for (int j = 0; j < words.length; j++) {
				if (words[i].equals(words[j])) {
					c++;
					anagram += words1[j] + ",";
				}
			}
anagram=anagram.substring(0, anagram.length()-1);
			if (c > 1) {
				if (!(list.contains(anagram)))
					list.add(anagram);
			}
		}

		return list;
	}

}
